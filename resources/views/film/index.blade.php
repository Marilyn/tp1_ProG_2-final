@extends('layouts.app')

@section('titre', 'Accueil ')

@section('content')

    <div class="flex centered">
        <h1>- Tous les films -</h1>
    </div>

        <div class="row padding-top">

            @if (empty($films))
                Il n'y a pas de film.
            @endif

            @foreach($films as $film)

            <div id="index-card">

                <div class="col s12 m6 l3 flex centered">

                    <div class="card large">

                        <div class="card-image">

                            <img src="img/film/{{ $film->image }}">

                        </div>

                        <div class="card-content">

                            <span class="card-title">

                                <li>{{ $film->titre }}</li></span>

                            <p><li>  {{ $film->synopsis }} </li></p>

                        </div>

                        <div class="card-action">

                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>

                            <a id="btn-detail" class="waves-effect waves-light btn white right"{{ link_to_route('film.show', 'Détail', $film->id) }}</a>

                        </div>

                    </div>

                </div>

            </div>

            @endforeach


        </div>

@endsection
