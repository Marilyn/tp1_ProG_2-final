@extends('layouts.app')

@section('titre', 'Modifier-film')

@section('content')


<div id="modif-film" class="row">


    <h2>Modifier le film {{ $film->titre }}</h2>

        <div class="col s12 m6 l6">

            {!! Form::model($film, ['method' => 'PATCH', 'files' => 'true', 'route' => ['film.update', $film->id]]) !!}

            {!! Form::label('titre','Titre: ') !!}
            <br>
            {!! Form::text('titre') !!} <br>

            {!! Form::label('annee','Année de production: ') !!} <br>
            {!! Form::text('annee') !!}<br>

            {!! Form::label('image','Image:') !!} <br>
            {!! Form::file('image') !!}<br>


            <div class="row">

                <div class="col s12 m6 l6">

                    {!! Form::label('id_classement','Classement:') !!} <br>

                    {!!  Form::select('id_classement', $classements) !!}

                </div>
                <div class="col s12 m6 l6">

                    {!! Form::label('duree','Durée: ') !!}<br>
                    {!! Form::text('duree') !!}<br>
                </div>
            </div>

        </div>

        <div class="col s12 m6 l6">

            {!! Form::label('synopsis','Synopsis: ') !!}<br>
            {!! Form::textarea('synopsis') !!}<br>

            {!! Form::label('acteurs','Acteurs: ') !!}<br>
            {!! Form::text('acteurs') !!}<br>


            {!! Form::submit('Modifier', array('class' => 'btn bouton-submit right')) !!}

            {!! Form::close() !!}

            @if (count($errors) > 0)
                <ul style="color:red">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

        </div>
</div>



@stop
