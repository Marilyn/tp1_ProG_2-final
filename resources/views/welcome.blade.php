@extends('layouts.app')

@section('titre', 'Accueil ')

@section('content')

<div id="welcome">

    <div class="flex centered" id="bienvenue">

        <h1>- Bienvenue -</h1>

    </div>

    <div class="row padding-top-wel flex centered">

        @if (empty($films))
            Il n'y a pas de film.
        @endif

        @foreach($films as $film)

                <div class="col s12 m6 l3 flex centered">

                    <div class="card">

                        <div class="par-dessus">



                            <div class="col">
                                <p>{{ $film->classement->nom }}</p>
                            </div

                            <p class="col">{{ $film->duree.' min' }}</p>

                        </div>

                        <div class="card-image">

                            <img src="img/film/{{ $film->image }}">



                        </div>

                        <div class="card-content" id="accueil-synop">

                            <div class="row">

                                <div class="card-title col">
                                    <p>{{ $film->titre }}</p>
                                </div>




                            </div>

                            <p>  <?php  $synop100 = $film->synopsis;

                                $synop100 = substr($synop100,0,100).' ...';

                                echo $synop100;

                                ?>
                            </p>

                        </div>

                        <div class="card-action">
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>
                            <span><i class="material-icons">star</i></span>

                        <a id="btn-detail" class="waves-effect waves-light btn white right"{{ link_to_route('film.show', 'Détail', $film->id) }}</a>
                        </div>

                    </div>

                </div>

        @endforeach

    </div>

</div>
@endsection
