<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;

class Film extends Model
{

    public function critiques()
    {
        return $this->hasMany('App\Critique', 'id_film');
    }
    public function classement()
    {
        return $this->belongsTo('App\Classement', 'id_classement');
    }
    
    protected $table = 'films';
    protected $fillable = [
        'titre', 'annee', 'image','duree', 'synopsis', 'acteurs', 'id_classement'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at',
    ];
}
