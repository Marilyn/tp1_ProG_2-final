<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Critique;
use DB;

use App\Http\Requests;

use App\Http\Requests\CreateCritiqueRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class CritiqueController extends Controller
{

    function index()
    {
     return view('welcome');
    }


// maude etoiles *****************************************************


//    public static function filmsAvecEtoiles($films)
//    {
//        foreach ($films as $cle => $film) {
//            $etoiles = CritiqueController::nbEtoiles($film->id);
//            $films[$cle]['etoiles'] = $etoiles->moyenne;
//        }
//        return $films;
//    }
//    public static function nbEtoiles($id)
//    {
//        $critique = DB::table('critiques')
//            ->select(DB::raw('ROUND(AVG(vote)) as moyenne'))
//            ->where('id_film', '=', $id)
//            ->get();
//        return $critique[0];
//    }
//
//    function miseAJour(Request $request)
//    {
//        $donnees = $request->all();
//        $id_utilisateur = $request->user()->id;
//        $deja = CritiqueController::dejaVote($id_utilisateur, $donnees['id']);
//        if (!empty($deja)) {
//            $etoiles = CritiqueController::nbEtoiles($donnees['id']);
//            return response()->json([
//                'etat' => 'erreur',
//                'message' => 'Vous avez déja voté pour ce film',
//                'etoiles' => $etoiles->moyenne,
//            ]);
//        }
//        $critique = new Critique();
//        $critique->vote = $donnees['etoiles'];
//        $critique->id_film = $donnees['id'];
//        $critique->id_utilisateur = $id_utilisateur;
//        $critique->save();
//        $etoiles = CritiqueController::nbEtoiles($donnees['id']);
//        return response()->json([
//            'etat' => 'succes',
//            'message' => 'Vous avez voté avec succès...',
//            'id' => $donnees['id'],
//            'etoiles' => $etoiles->moyenne
//        ]);
//    }
//
//    function dejaVote($id_utilisateur, $id_film)
//    {
//        $resultat = DB::table('critiques')
//            ->where([
//                ['id_utilisateur', '=', $id_utilisateur],
//                ['id_film', '=', $id_film],
//            ])->get();
//        return $resultat;
//    }

    // fin maude etoiles *****************************************************

    function store(CreateCritiqueRequest $request)
    {

        $donnees = $request->all();
        $critique = new Critique();
        //$critique->vote = $donnees['vote'];
        $critique->commentaire = $donnees['commentaire'];
        $critique->id_film = $donnees['id_film'];
        $critique->id_utilisateur = Auth::user()->id;

        $critique->save();

        flash()->success('Votre critique à été Ajoutée!'); // success() ajoute une classe qui donne la couleur verte à la box message

        return Redirect::to('/');

// code a maude pour etoile
//
//        $critique = DB::table('critiques')
//            ->select(DB::raw('SUM(vote) as vote, COUNT(vote) as count'))
//            ->where('id_film', '=', $id_film)
//            ->get();


    }

    function update($id, CreateCritiqueRequest $request)
    {

        $critique = Critique::findOrFail($id);

        $donnees = $request->all();
        $critique->commentaire = $donnees['commentaire'];
        $critique->id_film = $donnees['id_film'];

        $critique->save();

        $idUserCritiques = Critique::select('id_utilisateur')->where('id_film', $id)->distinct()->pluck('id_utilisateur');

        $film = $critique->id_film;

        flash()->success('Votre critique à été Modifiée!'); // success() ajoute une classe qui donne la couleur verte à la box message

        return redirect::action('FilmController@show', compact('critique', 'film', 'idUserCritiques'));
    }
}
