<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Critique extends Model
{

    public function user()
    {
        return $this->belongsTo('App\User', 'id_utilisateur');
    }

    protected $table = 'critiques';
    protected $fillable = [
        'vote', 'commentaire', 'id_film', 'id_utilisateur'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at',
    ];
}
